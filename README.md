# __Bridgewater Community Lions:__  
<!-- CI Badges go here. -->
This repository comprises the files that form the proposed website for the Bridgewater Community Lions.

<br>
<div align="center">
    <img alt="Image of the proposed BCLC homepage." src="https://dl.michaelpmiddleton.com/static/bclc.png" />
</div>
<br>

## _What's Up with the Branches?_
The different branches are used as follows:

| Branch | Purpose |
| ------: | ------ |
| **master** | Currently live on the website. Merges are squashed from *dev*. |
| **dev** | Upcoming, completed feautres. Merges are squashed from *hyper_dev* | 
| **hyper_dev** | Feature development. Only ever live on my sandbox. | 

<br><br>

## _Want to Fork?_
Please feel free to use my code as a reference for your own work. With that said, at this time I am requesting that you do not _copy_ this code. Thank you for your understanding and happy hacking!
<br><br>

## _Got Questions?_ 
I'm happy to answer them! Just [send me an email](mailto:mp.middleton@outlook.com)!
