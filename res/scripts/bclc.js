/*
    FILE:       bclc.js
    AUTHOR:     mmiddleton
    DATE:       15 MAY 2019
*/
// Variables:
var navbar = null;          // Main navbar of the website. 
var navbar_title = null;    // Main 'p' element with club title. 
var navbar_links = null;    // Div containing main links (eg. Events, About, etc.)
// ^^^^ Properties of above will change change via script/css depending on where on the page the viewport is located.
var sidebar = null          // Side navbar that is used on mobile devices. (Scripts are called in-line.)


// NavbarStandardView ():      Makes the title visible and adds the *visible* bar components of navbar
function NavbarStandardView () {
    navbar.addClass ("w3-card w3-white");
    navbar.children ("div").removeClass ("w3-opacity");
    navbar_links.removeClass ("w3-text-white");
    navbar_title.show ();
}



// NavbarLandingView ():      Makes the title invisible and removes the *visible* bar components of navbar
function NavbarLandingView () {
    navbar.removeClass ("w3-card w3-white");
    navbar.children ("div").addClass ("w3-opacity");
    navbar_links.addClass ("w3-text-white");
    navbar_title.hide ();
}



// OnReady ():      JQUERY - Called when the DOM is completely loaded.
$(function (){
    sidebar = $("#bclc-sidebar");
    navbar = $("#bclc-navbar");
    navbar_title = $("#club-name");
    navbar_links = navbar.children ("div"). children ("div");
  
    
    // OnScroll ():     Whenever the window is scrolled, check to see if we need to change Navbar view type:
    $(window).scroll(function () {
        if ($(window).scrollTop () > 500) {
            NavbarStandardView ();
        }
        else {
            NavbarLandingView ();  
        }
    });

});