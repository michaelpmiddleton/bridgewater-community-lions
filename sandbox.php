<?php
/*
	FILE:		sandbox.php
	AUTHOR:		mmiddleton
	DATE:		14 MAY 2019

	DESCRIPTION:
	This is the static landing page that a user will land on when they reach the site.    float: left;
    padding: 3vh 1vh;

*/
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home Page | Bridgewater Community Lions Club</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="res/img/favicon.ico" type="image/x-icon"/>

	<link rel="stylesheet" href="res/stylesheets/w3.css">
	<link rel="stylesheet" href="res/stylesheets/bclc.css">

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
	<script type="text/javascript" src="res/scripts/bclc.js"></script>
</head>


<body>
<!-- Navbar (sit on top) -->
<div class="w3-top w3-bar" id="bclc-navbar">
	<a href="#home" class="w3-bar-item w3-button w3-wide" style="background-color: transparent"><img id="hero" src="res/img/lci.png" alt="Lions Club International Logo" /></a>
	<p id="club-name" class="w3-xlarge w3-hide-small ">Bridgewater Community Lions Club</p>
	<div class="w3-opacity">
		<!-- Right-sided navbar links -->
		<div class="w3-right w3-hide-medium w3-hide-small w3-text-white" style="padding: 3vh">
			<a href="#about" class="w3-bar-item w3-button">ABOUT</a>
			<a href="#contact" class="w3-bar-item w3-button">CONTACT</a>
			<a href="#events" class="w3-bar-item w3-button">EVENTS</a>
			<a href="#news" class="w3-bar-item w3-button">NEWS</a>
			<a href="#projects" class="w3-bar-item w3-button">PROJECTS</a>
		</div>
		<!-- Hide right-floated links on small screens and replace them with a menu icon -->

		<a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="sidebar.show ()">
			<i class="fa fa-bars"></i>
		</a>
	</div>
</div>

<!-- Sidebar on small screens when clicking the menu icon -->
<nav class="w3-sidebar w3-bar-block w3-black w3-card w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="bclc-sidebar">
	<div onlclick="sidebar.hide ()"></div>  <!-- This is to add the abilty to click out of a menu without clicking the close icon or a link -->
	<a href="javascript:void(0)" onclick="sidebar.hide ()" class="w3-bar-item w3-button w3-large w3-padding-16">Close &times;</a>
	<a href="#about" onclick="sidebar.hide ()" class="w3-bar-item w3-button">ABOUT</a>
	<a href="#contact" onclick="sidebar.hide ()" class="w3-bar-item w3-button">CONTACT</a>
	<a href="#events" onclick="sidebar.hide ()" class="w3-bar-item w3-button">EVENTS</a>
	<a href="#news" onclick="sidebar.hide ()" class="w3-bar-item w3-button">NEWS</a>
	<a href="#projects" onclick="sidebar.hide ()" class="w3-bar-item w3-button">PROJECTS</a>
</nav>

<!-- Header with full-height image -->
<header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">
	<div class="w3-display-left w3-text-white" style="padding:48px">
		<span class="w3-jumbo w3-hide-small">Bridgewater Community Lions Club</span><br>
		<span class="w3-xxlarge w3-hide-large w3-hide-medium">Serving our Community, Serving the World.</span><br>
		<span class="w3-large">We're offering needed service in our local community and making a difference all over the world.</span>
		<p><a href="#about" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Learn More Today!</a></p>
	</div> 
</header>


<div class="w3-padding-large">
	<!-- About Section -->
	<div class="w3-container w3-center" id="about">
		<h2 class="w3-left">Our Global Service Goals</h2>
		<div class="w3-row-padding w3-center" style="margin-top:64px">
			<div class="w3-col l1 w3-hide-small">
				<p>&nbsp;</p>
			</div>
			<div class="w3-col m3 l2">
				<img style="width: 100%;" src="res/img/diabetes.png" alt="LCI official icon for Diabates global service initiative" />
				<p class="w3-large">Diabetes</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
			<div class="w3-col m3 l2">
				<img style="width: 100%;" src="res/img/vision.png" alt="LCI official icon for Vision global service initiative" />
				<p class="w3-large">Vision</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
			<div class="w3-col m3 l2">
				<img style="width: 100%;" src="res/img/hunger.png" alt="LCI official icon for Hunger global service initiative" />
				<p class="w3-large">Hunger</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
			<div class="w3-col m3 l2">
				<img style="width: 100%;" src="res/img/environment.png" alt="LCI official icon for Environment global service initiative" />
				<p class="w3-large">Environment</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
			<div class="w3-col m3 l2">
				<img style="width: 100%;" src="res/img/youth.png" alt="LCI official icon for Youth global service initiative" />
				<p class="w3-large">Youth</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
		</div>

	</div>

	<!-- Team Section -->
	<div class="w3-container" id="team">
		<h2>Signature Events</h2  >
		<div class="w3-row-padding" style="margin-top:64px">
			<div class="w3-col l3 m6 w3-margin-bottom">
				<div class="w3-card">
					<img src="res/img/placeholder.png" alt="John">
					<div class="w3-container">
						<p class="bclc-banner bclc-environment">Environment</p>
						<h3>Recyclerama</h3>
						<p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
						<p><button class="w3-button w3-light-grey w3-block">Learn More</button></p>
					</div>
				</div>
			</div>
			<div class="w3-col l3 m6 w3-margin-bottom">
				<div class="w3-card">
					<img src="res/img/placeholder.png" alt="Jane">
					<div class="w3-container">
						<p class="bclc-banner bclc-environment">Environment</p>
						<h3>Town Clean-Up Day</h3>
						<p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
						<p><button class="w3-button w3-light-grey w3-block">Learn More</button></p>
					</div>
				</div>
			</div>
			<div class="w3-col l3 m6 w3-margin-bottom">
				<div class="w3-card">
					<img src="res/img/placeholder.png" alt="Mike">
					<div class="w3-container">
					<p class="bclc-banner bclc-youth">Youth</p>
						<h3>Easter-Egg Hunt</h3>
						<p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
						<p><button class="w3-button w3-light-grey w3-block">Learn More</button></p>
					</div>
				</div>
			</div>
			<div class="w3-col l3 m6 w3-margin-bottom">
				<div class="w3-card">
					<img src="res/img/placeholder.png" alt="Dan">
					<div class="w3-container">
					<p class="bclc-banner bclc-vision">Vision</p>
						<h3>Eye-Glasses Recycling</h3>
						<p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
						<p><button class="w3-button w3-light-grey w3-block">Learn More</button></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Contact Section -->
	<div class="w3-container w3-light-grey" id="contact">
		<h3 class="w3-center">CONTACT</h3>
		<p class="w3-center w3-large">Lets get in touch. Send us a message:</p>
		<div style="margin-top:48px">
			<p><i class="fa fa-map-marker fa-fw w3-xxlarge w3-margin-right"></i> Chicago, US</p>
			<p><i class="fa fa-phone fa-fw w3-xxlarge w3-margin-right"></i> Phone: +00 151515</p>
			<p><i class="fa fa-envelope fa-fw w3-xxlarge w3-margin-right"> </i> Email: mail@mail.com</p>
			<br>
			<form action="/action_page.php" target="_blank">
				<p><input class="w3-input w3-border" type="text" placeholder="Name" required name="Name"></p>
				<p><input class="w3-input w3-border" type="text" placeholder="Email" required name="Email"></p>
				<p><input class="w3-input w3-border" type="text" placeholder="Subject" required name="Subject"></p>
				<p><input class="w3-input w3-border" type="text" placeholder="Message" required name="Message"></p>
				<p>
					<button class="w3-button w3-black" type="submit">
						<i class="fa fa-paper-plane"></i> SEND MESSAGE
					</button>
				</p>
			</form>
			<!-- Image of location/map -->
			<img src="res/img/placeholder.png" class="w3-image w3-greyscale" style="width:100%;margin-top:48px">
		</div>
	</div>
</div>
<!-- Footer -->
<footer class="w3-center w3-black w3-padding-64">
	<a href="#home" class="w3-button w3-light-grey"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
	<div class="w3-xlarge w3-section">
		<i class="fa fa-facebook-official w3-hover-opacity"></i>
		<i class="fa fa-instagram w3-hover-opacity"></i>
		<i class="fa fa-snapchat w3-hover-opacity"></i>
		<i class="fa fa-pinterest-p w3-hover-opacity"></i>
		<i class="fa fa-twitter w3-hover-opacity"></i>
		<i class="fa fa-linkedin w3-hover-opacity"></i>
	</div>
	<p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-text-green">w3.css</a></p>
</footer>
</body>
</html>